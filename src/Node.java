/**
 * Kasutatud materjalid:
 * http://cs.brown.edu/cgc/java3.datastructures.net/source/    chapter 6
 * http://moderntone.blogspot.com.ee/2013/08/a-general-algorithm-for-creating-tree.html
 * http://www.sunshine2k.de/coding/java/SimpleParser/SimpleParser.html
 */

import java.util.*;

public class Node {

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {

      name = n;
      firstChild = d;
      nextSibling = r;
   }

   public static void errorCheck (String s) {
      if (s.length() == 0)
         throw new RuntimeException("Empty tree " + s);
      if (s.contains("\t"))
         throw new RuntimeException("String contains a tab space " + s);
      if (s.contains(" "))
         throw new RuntimeException("String contains empty whitespaces " + s);
      if (s.contains("()"))
         throw new RuntimeException("String contains an empty subtree " + s);
      if (s.contains("((") && s.contains("))"))
         throw new RuntimeException("String contains two brackets " + s);
      if (s.contains(",,"))
         throw new RuntimeException("String contains two commas " + s);
      if (s.contains(",") && !s.contains("(") && !s.contains(")"))
         throw new RuntimeException("String contains two roots " + s);
   }

   public static Node parsePostfix (String s) {

      errorCheck(s);

      String[]chars = s.split("");

      Stack<Node> stack = new Stack();

      Node newNode = new Node(null, null, null);

      boolean rootReplace = false;

      for (int i = 0; i < chars.length; i++) {
         String newChars = chars[i].trim();
         if (newChars.equals("(")) {
            if (rootReplace) {
               throw new RuntimeException("Replacing root in string " + s);
            }
            stack.push(newNode);
            newNode.firstChild = new Node(null, null, null);
            newNode = newNode.firstChild;
            if (chars[i+1].trim().equals(",")) {
               throw new RuntimeException("Comma in string after node " + s);
            }

         } else if (newChars.equals(")")) {
            newNode = stack.pop();
            if (stack.size() == 0) {
               rootReplace = true;
            }

         } else if (newChars.equals(",")) {
            if (rootReplace) {
               throw new RuntimeException("Replacing root in string " + s);
            }

            newNode.nextSibling = new Node(null, null, null);
            newNode = newNode.nextSibling;
         } else {
            if (newNode.name == null) {
               newNode.name = newChars;
            } else {
               newNode.name += newChars;
            }
         }
      }
      return newNode;
   }

   public String leftParentheticRepresentation() {
      StringBuilder strbld = new StringBuilder();
      strbld.append(this.name);

      if (this.firstChild != null) {
         strbld.append("(");
         strbld.append(this.firstChild.leftParentheticRepresentation());
         strbld.append(")");
      }

      if (this.nextSibling != null) {
         strbld.append(",");
         strbld.append(this.nextSibling.leftParentheticRepresentation());
      }
      return strbld.toString();
   }

}

